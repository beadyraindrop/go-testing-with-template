# testinggoprojectcli

**This project template is created based on the "structured template". More info about usage: [Template repository](https://bitbucket.org/be-mobile/go-project-template-structured).**

< *Add a clear, but short *description* of what can be found in the repository. Use your "code request" description for example.* >

## Getting started

### Mac/Linux

```bash
make build && ./output/bin/testinggoprojectcli
```

### Windows

```powershell
make build GOOS=windows; .\output\bin\testinggoprojectcli.exe
```

## Deployment

### Environment variables

See `/configs/.env.localhost`

| Env variable | Description | Default value |
|-|-|-|
| CONFIG_LOGLEVEL | level for generated logs | `info` |

## Authors

* 

## Changelog

[changelog.md](changelog.md)
