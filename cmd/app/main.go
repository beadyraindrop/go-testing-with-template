package main
	
import (
	"fmt"
    "net/http"
	"encoding/json"
	"bytes"
)

type Company struct {
	Name string `json:"name"`
	Employees int `json:"employees"`
}


func main() {

	http.HandleFunc("/endpointget", endpointgetHandler)

	http.HandleFunc("/endpointpost", endpointpostHandler)

    http.ListenAndServe(":8090", nil)

}

func endpointgetHandler(w http.ResponseWriter, r *http.Request) {
	company := Company{
		Name: "Be-Mobile",
		Employees: 146}

	
	json.NewEncoder(w).Encode(company)
	w.Header().Set("Content-Type", "application/json")
}

func endpointpostHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
		case "POST":
			buf := new(bytes.Buffer)
			buf.ReadFrom(r.Body)
			newStr := buf.String()
		
			fmt.Printf(newStr) // This prints the body


		default:
			fmt.Fprintf(w, "Only POST requests are allowed on the endpoint /endpointpost")
		}
}